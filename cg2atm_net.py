import random

import numpy as np
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils

#import MDAnalysis as mda

class AtomicStructure():

    def __init__(self,system,topfile,trajfile):

        self.system = system
        self.topfile = topfile
        self.trajfile = trajfile

        self.u = mda.Universe(topfile,trajfile)  # always start with a Universe
        #print(list(u.atoms.residues))
        #print(list(u.atoms.segments))

        # subsystem is what we are interested in e.g. exclude solvent
        # this divided into molecules and then into groups
        self.subsystem = self.u.select_atoms('all')
        self.grps_per_mol = 1
        self.molecules = []
        self.groups = []

        self.atom_scale = 10.0
        self.CG_scale = 20.0
        self.im_size = 64

    def get_traj_length(self):

        return len(self.u.trajectory)

    def set_scales(self,atom_scale,CG_scale):

        self.atom_scale = atom_scale
        self.CG_scale = CG_scale

    def image_to_coords_v1(self,image):
        ''' Write a coordinate set corresponding to a
        single image.
        Image contains vectors with respect to the first atom,
        so we need to read in structure to get starting point.
        '''

        print(image)

        # skip to start of test frames
        # only works if do before select_atoms
        self.u.trajectory[16]

        if self.system == 'aladp':
            peptide = self.u.select_atoms('segid A')
        curr_coords = peptide[0].position
        print(curr_coords)

        r = np.zeros(3)
        max_len = 20

        #TODO losing last H
        for i in range(21):
            cx = image[0][0][3*i]
            cy = image[1][0][3*i]
            cz = image[2][0][3*i]

            r[0] = max_len * (cx - 0.5)
            r[1] = max_len * (cy - 0.5)
            r[2] = max_len * (cz - 0.5)

            peptide[i+1].position = curr_coords + r
            curr_coords = peptide[i+1].position
            print(curr_coords)

        peptide.write("backmapped.gro")

    def encode_dcd_data_v1(self,nframes):
        ''' Encode a trajectory in u, and return in XT, XS '''

        if self.system == 'aladp':
            peptide = self.u.select_atoms('segid A')
        beads = self.u.select_atoms('segid A and (name CA or name CL or name CR)')
        # length of atomistic array used to generate image
        atom_dim = int(len(peptide.atoms)*3 - 6)
        print(len(peptide.atoms),len(beads.atoms),atom_dim)

        # need to fix this for more general case
        if atom_dim <= 64:
            XT = torch.zeros([nframes,3,1,64])
            XS = torch.zeros([nframes,3,1,64])
        else:
            print("too many distances")
            exit(1)

        # estimate maximum distance for RGB scaling
        # mda.analysis.distances.self_distance_array(peptide)
        max_len = 20

        # select nframes from trajectory
        for f,ts in enumerate(self.u.trajectory):     # iterate through all frames

            i = 0
            for i1 in range(len(peptide.atoms)):
                for i2 in range(i1+1,i1+4):
                    if i2 < len(peptide.atoms):
                        r = peptide[i2].position - peptide[i1].position
                        cx = r[0]/max_len + 0.5
                        cy = r[1]/max_len + 0.5
                        cz = r[2]/max_len + 0.5
#                    print("frame = {0}: i1={1},i2={2},cx={3},cy={4},cz={5}".format(
#                        ts.frame, i1, i2, cx, cy, cz))
                        XT[f,0,0,i] = cx
                        XT[f,1,0,i] = cy
                        XT[f,2,0,i] = cz
                        i += 1

            i = 0
            for i1,bd1 in enumerate(beads):
                for i2,bd2 in enumerate(beads):
                    if i2 > i1:
                        r = bd2.position - bd1.position # end-to-end vector from atom positions
                        cx = r[0]/max_len + 0.5
                        cy = r[1]/max_len + 0.5
                        cz = r[2]/max_len + 0.5
#                    print("frame = {0}: bd1={1},bd2={2},cx={3},cy={4},cz={5}".format(
#                        ts.frame, bd1.name, bd2.name, cx, cy, cz))
                        XS[f,0,0,i:i+20] = cx
                        XS[f,1,0,i:i+20] = cy
                        XS[f,2,0,i:i+20] = cz
                        i += 20

        return XT, XS

    def image_to_coords_v2(self,image_cg,filename_cg,image,filename):
        ''' Write a coordinate set corresponding to a single image.
        The decoding requires the CoM of each group, which is also
        the CG bead position. This is back-calculated from image_CG.
        '''

        # skip to start of test frames
        # only works if do before select_atoms
        # This is now only used to get overall CoM for each molecule.
        self.u.trajectory[16]
        #com_ov = self.subsystem.center_of_mass()

        if self.system == 'aladp':
            x_dupl = 4
        elif self.system == 'sds60':
            x_dupl = 1
        r = np.zeros(3)

        # create new universe for CG coordinates
        cgu = mda.Universe.empty(len(self.molecules)+len(self.groups),trajectory=True)
        cgu.atoms.dimensions = (79.662, 79.662, 79.662, 90.00, 90.00, 90.00)
        
        i = 0
        for igp,gp in enumerate(self.groups):
            # This assumes access to atomic coordinates for this frame.
            # For inference, would need to get from CG coordinates.
            if igp % self.grps_per_mol == 0:
                com_ov = self.molecules[int(igp/self.grps_per_mol)].center_of_mass()
                cgu.atoms[int(igp/self.grps_per_mol)].position = com_ov

            # COM of group is position of bead and the reference position
            # for atoms in this group
            cx_bd = image_cg[0][0][i]
            cy_bd = image_cg[1][0][i]
            cz_bd = image_cg[2][0][i]
            
            r[0] = self.CG_scale * (cx_bd - 0.5)
            r[1] = self.CG_scale * (cy_bd - 0.5)
            r[2] = self.CG_scale * (cz_bd - 0.5)
            
            com = com_ov + r
            cgu.atoms[len(self.molecules)+igp].position = com

            for i1 in range(len(gp.atoms)):
                cx = image[0][0][i]
                cy = image[1][0][i]
                cz = image[2][0][i]

                r[0] = self.atom_scale * (cx - 0.5)
                r[1] = self.atom_scale * (cy - 0.5)
                r[2] = self.atom_scale * (cz - 0.5)            

                gp[i1].position = com + r
                
                i += x_dupl
            
        self.subsystem.write(filename)
        cgu.atoms.write(filename_cg)

    def encode_dcd_data_v2(self,nframes):
        ''' Encode a trajectory in u, and return in XT, XS
        This follows the Scheme 1 encoding from Li et al (2020)
        '''

        if self.system == 'aladp':
            # this bit is specific to aladp example
            self.subsystem = self.u.select_atoms('segid A')
            self.groups.append(self.u.select_atoms('segid A and (name CL or name CLP or name OL)'))
            self.groups.append(self.u.select_atoms('segid A and (name NL or name HL or name CA or name CB or name CRP or name OR)'))
            self.groups.append(self.u.select_atoms('segid A and (name NR or name HR or name CR)'))
            # this is factor to expand the features to fill the image
            x_dupl = 4
        elif self.system == 'sds60':
            self.subsystem = self.u.select_atoms('resname SDS and not name H*')
            self.im_size = 64
            self.grps_per_mol = 5
            # 60 SDS, solvent removed
            for r in range(3):
                self.molecules.append(self.subsystem.select_atoms('resid %d' % (r+1)))
                self.groups.append(self.molecules[-1].select_atoms('name S or name OS1 or name OS2 or name OS3'))
                self.groups.append(self.molecules[-1].select_atoms('name OS4 or name C1 or name C2 or name C3'))
                self.groups.append(self.molecules[-1].select_atoms('name C4 or name C5 or name C6'))
                self.groups.append(self.molecules[-1].select_atoms('name C7 or name C8 or name C9'))
                self.groups.append(self.molecules[-1].select_atoms('name C10 or name C11 or name C12'))
                # this is factor to expand the features to fill the image
                x_dupl = 1

        # estimate maximum distance for RGB scaling
        # mda.analysis.distances.self_distance_array(peptide)
        self.set_scales(7,14)

        # length of atomistic array used to generate image
        atom_dim = 0
        for gp in self.groups:
            atom_dim += len(gp.atoms)
        atom_dim = atom_dim*x_dupl    
        print(len(self.groups),atom_dim)
        # need to fix this for more general case
        if atom_dim <= self.im_size:
            XT = torch.zeros([nframes,3,1,self.im_size])
            XS = torch.zeros([nframes,3,1,self.im_size])
        else:
            print("too many atoms to fit %sx%s image" % (self.im_size,self.im_size))
            exit(1)
        
        # select nframes from trajectory
        for f,ts in enumerate(self.u.trajectory):     # iterate through all frames

            if f >= nframes:
                break
        
            i = 0
            #com_ov = self.subsystem.center_of_mass()
            for igp,gp in enumerate(self.groups):
                if igp % self.grps_per_mol == 0:
                    com_ov = self.molecules[int(igp/self.grps_per_mol)].center_of_mass()
                    print("CoM of molecule %d is %s" % (int(igp/self.grps_per_mol),com_ov))
                # COM of group is position of bead and the reference position
                # for atoms
                com = gp.center_of_mass()

                # vector for bead
                r = com - com_ov
                cx_bd = r[0]/self.CG_scale + 0.5
                cy_bd = r[1]/self.CG_scale + 0.5
                cz_bd = r[2]/self.CG_scale + 0.5
            
                for i1 in range(len(gp.atoms)):
                    r = gp[i1].position - com
                    cx = r[0]/self.atom_scale + 0.5
                    cy = r[1]/self.atom_scale + 0.5
                    cz = r[2]/self.atom_scale + 0.5
                    print("frame = {0}: i1={1},cx={2:.2f},cy={3:.2f},cz={4:.2f},cx_bd={5:.2f},cy_bd={6:.2f},cz_bd={7:.2f}".format(
                        ts.frame, i1, cx, cy, cz, cx_bd, cy_bd, cz_bd))
                    XT[f,0,0,i:i+x_dupl] = cx
                    XT[f,1,0,i:i+x_dupl] = cy
                    XT[f,2,0,i:i+x_dupl] = cz

                    XS[f,0,0,i:i+x_dupl] = cx_bd
                    XS[f,1,0,i:i+x_dupl] = cy_bd
                    XS[f,2,0,i:i+x_dupl] = cz_bd

                    i += x_dupl

        return XT, XS

def get_dcd_data(system,nsamples,show=False):

    if system == 'aladp':
        psf_file = "data/aladp-solvated.psf"
        traj_file = "data/md1.dcd"
    elif system == 'sds60':    
        psf_file = "../../ya-wen/md-1_nosolv.pdb"
        traj_file = "../../ya-wen/md-1_nosolv.dcd"
    s = AtomicStructure(system,psf_file,traj_file)

    if s.get_traj_length() < nsamples:
        nframes = s.get_traj_length()
    else:
        nframes = nsamples
    
    XT, XS = s.encode_dcd_data_v2(nframes)

    XT = XT.repeat(1, 1, s.im_size, 1)
    XS = XS.repeat(1, 1, s.im_size, 1)
    print(XT.shape)
    print(XS.shape)

    # show input image pairs
    if show:
        nim = 5
        for i in range(nim):
            plt.subplot(nim,2,2*i+1)
            plt.imshow(np.transpose(XT[i], (1, 2, 0)))
            plt.subplot(nim,2,2*i+2)
            plt.imshow(np.transpose(XS[i], (1, 2, 0)))
        plt.show()

    test_split = int(nframes-(nframes*0.2))
    XT_test = XT[test_split:]
    XT = XT[:test_split]
    XS_test = XS[test_split:]
    XS = XS[:test_split]

    return s, XT, XT_test, XS, XS_test


def get_data_yawen(aa_path, cg_path, frames=True, show=False):
    molecules = 60
    atm_per_mol = 42
    no_per_frame = 60 * 42   # 60 molecules per frame x 42 atoms per molecue -> 2520 atoms per frame

    def read(path):
        f = open(path, 'r')
        data = f.read()
        f.close()
        return data

    # transform = transforms.Compose([
    #     transforms.ToPILImage(),
    #     transforms.ToTensor(),
    #     transforms.Normalize((0.5,), (0.5,))
    # ])

    # read data
    aa = read(aa_path)
    cg = read(cg_path)
    aa = aa.replace(' ', '').split('\n')[:-1]
    cg = cg.split('\n')[:-1]

    aa_data = []
    cg_data = []
    if frames:
        # convert to frames
        step = no_per_frame
    else:
        # convert to molecules (memory saving for prototyping)
        step = 32#atm_per_mol
    for i in range(0, len(aa), step):
        # for each frame
        aa_data.append(aa[i:i+step])
        cg_data.append(cg[i:i+step])

    # to tensor
    XT = torch.from_numpy(np.asarray(aa_data, dtype=np.float32))
    XS = torch.from_numpy(np.asarray(cg_data, dtype=np.float32))
    # add y dimension (2D conversion)
    XT = torch.unsqueeze(XT, 1)
    XS = torch.unsqueeze(XS, 1)
    # repeat values along y axis
    XT = XT.repeat(1, XT.shape[-1], 1)  # 64, 1) # XT.shape[-1], 1)
    XS = XS.repeat(1, XS.shape[-1], 1)  # 64, 1) # XS.shape[-1], 1)
    # add channels (depth)
    XT = torch.unsqueeze(XT, 1)
    XS = torch.unsqueeze(XS, 1)

    print(XT.shape, XS.shape)
    if show:
        plt.imshow(XT[0, 0, :, :])
        plt.figure()
        plt.imshow(XS[0, 0, :, :])
        plt.show()

    nsamples = len(XT)
    test_split = int(nsamples-(nsamples*0.2))
    XT_test = XT[test_split:]
    XT = XT[:test_split]
    XS_test = XS[test_split:]
    XS = XS[:test_split]

    return XT, XT_test, XS, XS_test

def get_data_cg2atm(nsamples, natms=64, atmperbead=4, show=False):
    # random seed
    seed = random.randint(1, 10000)
    random.seed(seed)
    torch.manual_seed(seed)

    # transforms
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.5,), (0.5,))
    ])

    atms = torch.randn((nsamples, 3, 1, natms))
    cgs = torch.Tensor()
    for i in range(0, atms.shape[-1], atmperbead):
        bead = torch.mean(atms[:,:,:,i:i+atmperbead], dim=-1, keepdim=True).repeat(1, 1, 1, atmperbead)
        cgs = torch.cat((cgs, bead), -1)

    XT = atms.repeat(1, 1, atms.shape[-1], 1)
    XS = cgs.repeat(1, 1, atms.shape[-1], 1)
    print(XT.shape)
    print(XS.shape)
    if show:
        plt.imshow(np.transpose(XT[0], (1, 2, 0)))
        plt.figure()
        plt.imshow(np.transpose(XS[0], (1, 2, 0)))
        plt.show()

    test_split = int(nsamples-(nsamples*0.2))
    XT_test = XT[test_split:]
    XT = XT[:test_split]
    XS_test = XS[test_split:]
    XS = XS[:test_split]

    return XT, XT_test, XS, XS_test



def get_data_MNIST(show=False):
    # random seed
    seed = random.randint(1, 10000)
    random.seed(seed)
    torch.manual_seed(seed)

    # transforms
    transform = transforms.Compose([
        transforms.Resize(64),
        transforms.CenterCrop(64),
        transforms.ToTensor(),
        transforms.Normalize((0.5,), (0.5,))
    ])

    # test target data
    dataset_train = dset.MNIST('.', train=True, download=True, transform=transform)
    dataset_test = dset.MNIST('.', train=False, download=True, transform=transform)

    # PyTorch won't apply PIL resize transforms until 'next' function is called on the iterator
    # we want to push all data to GPU to speed the process, but can't do that with a generator
    # so just re-iterating data with next to grab the transformed images
    XT = torch.stack([data.repeat(1, 3, 1, 1).squeeze() for data, target in dataset_train][:200])
    XT_test = torch.stack([data.repeat(1, 3, 1, 1).squeeze() for data, target in dataset_test][:200])
    XT = XT.type(torch.float)
    XT_test = XT_test.type(torch.float)

    # test source data
    XS = torch.randn(XT.shape[0], 3, 1, 1)
    XS_test = torch.randn(XT_test.shape[0], 3, 1, 1)

    # shuffle data
    randperm = np.random.permutation(len(XT))
    XS, XT = XS[randperm], XT[randperm]

    print("X target:", XT.shape, "X_test target", XT_test.shape, "X source:", XS.shape, "X_test source:", XS_test.shape)

    if show:
        plt.figure(figsize=(8, 8))
        plt.title("Training Images")
        plt.imshow(np.transpose(vutils.make_grid(XT[:64].clone().detach().to("cpu"), padding=2, normalize=False),
                            (1, 2, 0)))
        plt.show()

    return XT, XT_test, XS, XS_test


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)


class Discriminator(nn.Module):
    def __init__(self, nc=3, ndf=64, imsize=64, verbose=False):
        super(Discriminator, self).__init__()
        self.nc = nc
        self.ndf = ndf
        self.imsize = imsize
        self.verbose = verbose

        self.encode1 = self.encode(self.nc, self.ndf, batchnorm=False)
        self.encode2 = self.encode(self.ndf, self.ndf*2)
        self.encode3 = self.encode(self.ndf*2, self.ndf*4)
        self.encode4 = self.encode(self.ndf*4, self.ndf*8)
        self.conv = nn.Conv2d(self.ndf * 8, 1, 4, 2, 1, bias=False)  #1, 0, bias=False)
        self.flatten = nn.Flatten()
        self.fc = nn.Linear(np.power(self.imsize//np.power(2,5),2), 1)  # (imsize//2^depth)^2dims
        self.actv = nn.Sigmoid()


    def encode(self, inp, out, batchnorm=True):
        conv = nn.Conv2d(inp, out, 4, 2, 1, bias=False)
        norm = nn.BatchNorm2d(out)
        actv = nn.LeakyReLU(0.2, inplace=True)
        if batchnorm:
            return nn.Sequential(conv, norm, actv)
        else:
            return nn.Sequential(conv, actv)

    def forward(self, x):
        if self.verbose:
            print("DISCRIMINATOR")
            print(x.shape)
        x = self.encode1(x)
        if self.verbose:
            print(x.shape)
        x = self.encode2(x)
        if self.verbose:
            print(x.shape)
        x = self.encode3(x)
        if self.verbose:
            print(x.shape)
        x = self.encode4(x)
        if self.verbose:
            print(x.shape)
        x = self.conv(x)
        if self.verbose:
            print(x.shape)
        x = self.flatten(x)
        if self.verbose:
            print(x.shape)
        x = self.fc(x)
        if self.verbose:
            print(x.shape)
        x = self.actv(x)
        if self.verbose:
            print(x.shape)

        return x


class Generator(nn.Module):
    def __init__(self, nc=3, ngf=64):
        super(Generator, self).__init__()
        self.nc = nc
        self.ngf = ngf

        self.encode1 = self.encode(self.nc, self.ngf, batchnorm=False)
        self.encode2 = self.encode(self.ngf, self.ngf*2)
        self.encode3 = self.encode(self.ngf*2, self.ngf*4)
        self.encode4 = self.encode(self.ngf*4, self.ngf*8)
        # self.decode1 = self.decode(self.nz, self.ngf*8, stride=1, padding=0)
        self.decode2 = self.decode(self.ngf*8, self.ngf*4)
        self.decode3 = self.decode(self.ngf*4, self.ngf*2)
        self.decode4 = self.decode(self.ngf*2, self.ngf)
        self.conv = nn.ConvTranspose2d(self.ngf, self.nc, 4, 2, 1, bias=False)
        self.actv = nn.Tanh()


    def encode(self, inp, out, batchnorm=True):
        conv = nn.Conv2d(inp, out, 4, 2, 1, bias=False)
        norm = nn.BatchNorm2d(out)
        actv = nn.LeakyReLU(0.2, inplace=True)
        if batchnorm:
            return nn.Sequential(conv, norm, actv)
        else:
            return nn.Sequential(conv, actv)

    def decode(self, inp, out, stride=2, padding=1):
        conv = nn.ConvTranspose2d(inp, out, 4, stride, padding, bias=False)
        norm = nn.BatchNorm2d(out)
        actv = nn.ReLU(True)
        return nn.Sequential(conv, norm, actv)

    def forward(self, x):
        # TODO need to stack encoder here
        x = self.encode1(x)
        x = self.encode2(x)
        x = self.encode3(x)
        x = self.encode4(x)
        x = self.decode2(x)
        x = self.decode3(x)
        x = self.decode4(x)
        x = self.conv(x)
        x = self.actv(x)
        return x

class UNet(nn.Module):
    def __init__(self, nc=3, ngf=64, verbose=False):
        super(UNet, self).__init__()
        self.nc = nc
        self.ngf = ngf
        self.verbose = verbose

        self.pool = nn.MaxPool2d(2, 2)  #, ceil_mode=False)

        self.encode1 = self.block(self.nc, self.ngf)
        self.encode2 = self.block(self.ngf, self.ngf*2)
        self.encode3 = self.block(self.ngf*2, self.ngf*4)
        self.encode4 = self.block(self.ngf*4, self.ngf*8)

        self.bottleneck = self.block(self.ngf*4, self.ngf*8)    ### DEPTH CHANGE

        self.up4 = nn.ConvTranspose2d(self.ngf*16, self.ngf*8, 2, 2)
        self.decode4 = self.block(self.ngf*16, self.ngf*8)
        self.up3 = nn.ConvTranspose2d(self.ngf*8, self.ngf*4, 2, 2)
        self.decode3 = self.block(self.ngf*8, self.ngf*4)
        self.up2 = nn.ConvTranspose2d(self.ngf*4, self.ngf*2, 2, 2)
        self.decode2 = self.block(self.ngf*4, self.ngf*2)
        self.up1 = nn.ConvTranspose2d(self.ngf*2, self.ngf, 2, 2)
        self.decode1 = self.block(self.ngf*2, self.ngf)

        self.conv = nn.Conv2d(self.ngf, self.nc, 3, 1, 1, bias=False)
        self.actv = nn.Tanh()



    def block(self, inp, out):
        return nn.Sequential(
            nn.Conv2d(inp, out, 3, 1, 1, bias=False),
            nn.BatchNorm2d(out),
            nn.ReLU(True),
            nn.Conv2d(out, out, 3, 1, 1, bias=False),
            nn.BatchNorm2d(out),
            nn.ReLU(True)
        )

    def forward(self, x):
        if self.verbose:
            print("GNERATOR")
            print(x.shape)
        x1 = self.encode1(x)
        if self.verbose:
            print(x1.shape)
        x2 = self.encode2(self.pool(x1))
        if self.verbose:
            print(x2.shape)
        x3 = self.encode3(self.pool(x2))
        if self.verbose:
            print(x3.shape)
        # x4 = self.encode4(self.pool(x3))
        # if self.verbose:
        #     print(x4.shape)
        b = self.bottleneck(self.pool(x3))  ### DEPTH CHANGE
        if self.verbose:
            print(b.shape)
        # y4 = self.up4(b)
        # if self.verbose:
        #     print(y4.shape)
        # y4 = torch.cat([y4, x4], dim=1)
        # if self.verbose:
        #     print(y4.shape)
        # y4 = self.decode4(y4)
        # if self.verbose:
        #     print(y4.shape)
        y3 = self.up3(b)       ### DEPTH CHANGE
        if self.verbose:
            print(y3.shape)
        y3 = torch.cat([y3, x3], dim=1)
        if self.verbose:
            print(y3.shape)
        y3 = self.decode3(y3)
        if self.verbose:
            print(y3.shape)
        y2 = self.up2(y3)
        if self.verbose:
            print(y2.shape)
        # ##### PATCH FOR NON POWER-2 (E.G. 64) LENGTH DATA #####
        # y2 = F.pad(y2, (0, 1, 0, 0), mode='replicate')
        y2 = torch.cat([y2, x2], dim=1)
        if self.verbose:
            print(y2.shape)
        y2 = self.decode2(y2)
        if self.verbose:
            print(y2.shape)
        y1 = self.up1(y2)
        if self.verbose:
            print(y1.shape)
        y1 = torch.cat([y1, x1], dim=1)
        if self.verbose:
            print(y1.shape)
        y1 = self.decode1(y1)
        if self.verbose:
            print(y1.shape)
        x = self.conv(y1)
        if self.verbose:
            print(x.shape)
        x = self.actv(x)
        if self.verbose:
            print(x.shape)
        return x


def train(XT, XS, dsc, gen, optimizerD, optimizerG, criterion, batch_size):
    for i, k in enumerate(range(batch_size, XS.shape[0], batch_size)):

        target = XT[k - batch_size:k].to(device)
        source = XS[k - batch_size:k].to(device)
        fake = gen(source)  # G(x)

        # Train Discriminator
        dsc.zero_grad()

        out = dsc(target).squeeze()
        lossD_real = criterion(out, torch.ones(target.shape[0]).to(device))    # D(x)
        lossD_real.backward()
        acc_Dx = out.mean().item()

        out = dsc(fake.detach()).squeeze()
        lossD_fake = criterion(out, torch.zeros(target.shape[0]).to(device))    # D(G(x))
        lossD_fake.backward()
        acc_DGx = out.mean().item()

        lossD = lossD_real + lossD_fake
        optimizerD.step()

        # Train Generator
        gen.zero_grad()
        out = dsc(fake).squeeze()
        lossG = criterion(out, torch.ones(target.shape[0]).to(device))   # 1-D(G(x))
        lossGCCE = nn.MSELoss(reduction='sum')(fake, target)  # torch.sum((fake - target)**2)
        lossG += lossGCCE
        lossG.backward()
        acc_1DGx = out.mean().item()
        optimizerG.step()

        # Output training stats
        if i % batch_size == 0:
            print('Loss_D: %.4f | Loss_G: %.4f | Acc D(x): %.4f | Acc D(G(x): %.4f | Acc 1-D(G(x)): %.4f'
                  % (lossD.item(), lossG.item(), acc_Dx, acc_DGx, acc_1DGx))
            # plt.imshow(
            #     np.transpose(vutils.make_grid(fake.clone().detach().to("cpu"), padding=2, normalize=False).cpu(),
            #                  (1, 2, 0)))
            plt.subplot(1,2,1)
            plt.imshow(np.transpose(fake[0].clone().detach().to("cpu"), (1,2,0)))
            plt.xticks([], [])
            plt.yticks([], [])
            plt.subplot(1,2,2)
            plt.imshow(np.transpose(target[0].clone().detach().to("cpu"), (1,2,0)))
            plt.xticks([], [])
            plt.yticks([], [])
            plt.draw()
            plt.pause(0.1)


def test(s, XT_test, XS_test):
    '''
    Predict atomic configurations for test set.
    XT are atomic, XS are coarse grained.
    XT_test is ground truth for the test set.
    '''
    
    test = gen(XS_test.to(device))
    plt.title("Test results")
    plt.subplot(1, 2, 1)
    plt.imshow(np.transpose(test[0].clone().detach().to("cpu"), (1, 2, 0)))
    plt.subplot(1, 2, 2)
    plt.imshow(np.transpose(XT_test[0].clone().detach().to("cpu"), (1, 2, 0)))
    plt.savefig("test_output.png")
    plt.close()

    s.image_to_coords_v2(XS_test[0],"cg_target_n.gro",XT_test[0],"backmapped_target_n.gro")
    s.image_to_coords_v2(XS_test[0],"cg_predicted_n.gro",test[0],"backmapped_predicted_n.gro")
    
    print("\n\nTest Error: %d" % nn.MSELoss(reduction='sum')(test, XT_test.to(device)))


def learn(s, XT, XT_test, XS, XS_test, dsc, gen, nepochs=500, batch_size=64, lr=0.001, beta1=0.5):

    criterion = nn.BCELoss()
    optimizerD = optim.Adam(dsc.parameters(), lr=lr, betas=(beta1, 0.999))
    optimizerG = optim.Adam(gen.parameters(), lr=lr, betas=(beta1, 0.999))

    for epoch in range(nepochs):
        print('Epoch: %d/%d' % (epoch, nepochs))
        train(XT, XS, dsc, gen, optimizerD, optimizerG, criterion, batch_size)
    test(s, XT_test, XS_test)


nframes = 20      # i.e. number of samples requested
batch_size = 10    # frames in each batch
nepochs = 10       # originally 500
lr = 0.0002
beta1 = 0.5

data_size = 100
imsize = 256
verbose = 0
show_input = 0

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

### ---------------- REPLACE DATA HERE
s = None
XT, XT_test, XS, XS_test = get_data_cg2atm(data_size, natms=imsize, show=show_input)
# XT, XT_test, XS, XS_test = get_data_yawen("AA-int-coord-100Frame.dat", "CG-int-coord-100Frame.dat", frames=False)

# Get the training and test arrays
# The structure s is needed for the final backmapping of predictions
# to coordinates
#s, XT, XT_test, XS, XS_test = get_dcd_data('sds60',nframes,show=show_input)
#imsize = s.im_size
### ---------------- REPLACE DATA HERE


#gen = Generator().to(device)
gen = UNet(nc=XT.shape[1], verbose=verbose).to(device)
dsc = Discriminator(nc=XT.shape[1], imsize=imsize, verbose=verbose).to(device)
gen.apply(weights_init)
dsc.apply(weights_init)
print(gen)
print(dsc)

learn(s, XT, XT_test, XS, XS_test, dsc, gen, nepochs, batch_size, lr, beta1)
